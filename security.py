import os.path
import base64

STATICFILES_DIRS = (
    os.path.join('static'),
)


class security():
    based_key = ''

    STATIC_URL = '/static/'

    def verify(self, i):
        return 1


class xauth():
    secret = 987456321
    user_key = 2145

    def verify(self, key):
        key = base64.b16decode(key)
        key = key / self.secret
        return key

    def valid(self, key):
        return self.verify(key) == self.user_key
