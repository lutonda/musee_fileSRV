import json
from settings import config
from security import xauth
from unittest import loader
import cherrypy
from flask import Flask, render_template, json, request
from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('templates'))
from cherrypy import tools


def error_page_404(status, message, traceback, version):
    return "404 Error!"


class HomeController(config,xauth):
    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def GetStates(self, **kwargs):
        xauth = cherrypy.request.headers.get('X-Auth-Token', None)
        xauth.valid(xauth)
        
        input_json = cherrypy.request.params.get('json')
        input_json = input_json.lower()

        with open('states.json', 'r') as chat_file:
            states_list = json.loads(chat_file.read())
            states_list = [s.lower() for s in states_list]
            return [s for s in states_list if input_json in s]

    @cherrypy.expose
    def home(self):
        tmpl = env.get_template('index.html')
        return tmpl.render(static=config.DOM_URL, target='World')


def start_server():
    cherrypy.tree.mount(HomeController(), '/')
    cherrypy.config.update({'error_page.404': error_page_404})
    cherrypy.config.update({'server.socket_port': 9000})
    cherrypy.engine.start()


if __name__ == '__main__':
    start_server()
